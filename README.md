# salesforce-integration

Documentation to integrate AheevaCCS within Salesforce.

## Requirements
1. Existing installation of AheevaCCS (v7.14.3+ or v8.1.1+)
2. Salesforce Lightning Experience (Essentials, Professional, Enterprise, Performance, Unlimited, or Developer Edition)

**Note**: Will not work on trial accounts

## Supported Browsers
1. Google Chrome (Version 87.0.4280.67 or above)

## Installation

<b>Note:</b> It is recommended that you test this integration on a sandbox environment prior to deploying it in production.

Please read the following instructions very carefully.

1. Log out of your salesforce console before proceeding with the next steps. This step is mandatory as the installation could seldom fail when you are trying to install the application while previously logged in.
2. Click on one of the links below depending on what kind of environment you are trying to install the application on.


   <b>For Production:</b> \
   <https://login.salesforce.com/packaging/installPackage.apexp?p0=04t4x000000VCoH>

   <b>For Sandbox:</b> \
   <https://test.salesforce.com/packaging/installPackage.apexp?p0=04t4x000000VCoH>

3. You will be directed to the salesforce login screen. Enter your credentials and click "Connect".
4. After validating your credentials, salesforce will redirect to the app installation screen.

   <b>Installation screen:</b>

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_1a.png)

5. Make a selection about which users you want the app to be available to. Ideally you would want to install the app only to few users. Choose the "Install for Specific Profiles" option for this use case.
6. Upon selecting the "Install for Specific Profiles" option, a "Select Specific Profiles" section will appear as shown below.

   <b>Profiles Section:</b>

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_2.png)

7. Choose the level of access you want to assign for each profile to the app.
8. Check "I acknowledge that I’m installing a Non-Salesforce Application that is not authorized for distribution as part of Salesforce’s AppExchange Partner Program." and click "Install".
9. The installation process will begin. Once installed, click on the "Done" button.

   <b>Installation:</b>

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_3.png)

   <b>Completed Installation:</b>

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_4.png)

## Post Installation
After the installation of the app, we now need to configure the application. The configuration process can be broken down into the following tasks.

1. Configuring the Call Center
2. Adding the OpenCTI telephone to the utility bar in your application view
3. Configuring the AheevaCCS Phone settings
4. Creating a Softphone Layout

### Configuring the Call Center

1. Log in to your salesforce environment.
2. Navigate to the "Setup" page by clicking on the gear icon on the header and selecting "Setup".

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_5.png)

3. In the "Quick Find" box, type "Call Centers". In the list that appears on the left side of the screen, choose "Call Centers".

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_6.png)

4. In the setup screen, click "Continue".

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_7.png)

5. In the next screen you will be shown a list of call centers installed in your salesforce environment. Click on "AheevaCCS".

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_8.png)

6. In the following screen, click on the "Add More Users" button.

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_10.png)

7. In the following screen click the "Find" button and choose the users you want the AheevaCCS Phone to be available to, and click on the "Add to Call Center" button.

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_11.png)

### Adding the OpenCTI telephone to the utility bar in your application view

1. Navigate to the "App Manager" section in the setup page.
2. A list of apps currently in use appears.
3. Click on the dropdown next to the app you want to add the AheevaCCS Phone to.
4. Click on the "Edit" option.

   Steps 1-4 are shown in the image below:

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_12.png)

5. In the "App Settings" page, click on the "Utility Items".
6. Add the "Open CTI Softphone" by using the "Add Utility Item" button.
7. From the properties pane, set the panel width to '350' and the panel height to '600' as shown below.
   
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_13.png)

8. Navigate to your app, preferrably in a new tab. You will find that the "AheevaCCS Phone" has been added to the utility bar at the bottom of your screen.
   
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_14.png)

### Configuring the AheevaCCS Phone settings

   1. Open the "Developer Console" from the menu on the header.
   
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_15.png)

   2. From the file menu, create a new static resource called "AheevaConfig" as shown below.
   
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_31.png)

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_32.png)
    
   3. Copy and paste the following block of code, replacing the existing contents of the file.

   ```javascript
   AHEEVA_HOST = 'https://beta.aheeva.dev';
   AHEEVA_IS_SOFTPHONE = true;
   AHEEVA_IS_NON_FORCED = true;
   ```

   4. For the variable "AHEEVA_HOST", set the value as your Agent Application URL. For the variable "AHEEVA_IS_SOFTPHONE", set the value as true if the agents use a softphone like MicroSIP or Zoiper to connect their extension; set this value as false if the agents use WEBRTC to connect their extension. For the variable "AHEEVA_IS_NON_FORCED", set this value as true if the inbound calls received by the agent are of type "Non Forced"; set this value as false if the inbound calls received by the agent are of type "Forced".

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_18.png)

   5. Save the file.

   6. From the file menu, create a VisualForce Page called "aheeva" as shown below.

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_33.png)

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_34.png)


   7. Copy and paste the following block of code, replacing the existing contents of the file.

   ```
   <apex:page>
       <style>.hasMotif{margin : 0px;}</style>
       <apex:stylesheet value="{!URLFOR($Resource.aheevaccs__MainStyles)}"/>
       <apex:includeScript value="{!$Resource.aheevaccs__OpenCTI}"/>
       <apex:includeScript value="{!$Resource.aheevaccs__UUID}"/>
       <apex:includeScript value="{!$Resource.aheevaccs__AheevaAppFramework}"/>
       <apex:includeScript value="{!$Resource.AheevaConfig}"/>
       <apex:includeScript value="{!$Resource.aheevaccs__MainScripts}"/>
       <span id="username" class="hidden">__crm__{!$User.Id}</span>
       <span id="password" class="hidden">{!$User.aheevaccs__External_Token__c}</span>
       <iframe onload="init()" allow="camera *;microphone *" id="aheeva" src="https://beta.aheeva.dev/salesforce?realm=aheeva" width="100%" height="554px" frameborder="0"></iframe>
   </apex:page>
   ```

   8. Change the value of the URL (as shown in the boxed area) to the URL of the Agent Application along with the tenant realm.

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_35.png)

   9.  Press "CTRL + S" to save the file, or click on the "Save" option from the file menu. You may now close the "Developer Console".

   The setup process is now complete.

## Creating a Connected App

   1. Click on "New Connected App" button on the LIghtning Experience App manager as shown below.

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_28.png)

   2. Fill in the details as shown below.

   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_29.png)

   3. Set the value for "Callback URL" as:
   
      ```
      https://{{AHEEVACCS_REST_SERVER_IP}}:{{AHEEVACCS_REST_SERVER_PORT}}/api/salesforce/oauth/tenant/{{AHEEVACCS_TENANT_ID}}/callback
      ```

      For example: 
      ```
      https://beta.aheeva.dev:4343/api/salesforce/oauth/tenant/1/callback
      ```
   
   4. Assign the specified API scopes as seen in the screenshot above. Click on the "Save button".

   5. After saving, from the screen that appears, copy down the "Consumer Key", "Consumer Secret" and the "Callback URL" values.
   
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_30.png)

## Integration with AheevaCCS Manager

   1.  Now, navigate to the "Salesforce" module inside the "Integrations" menu item in the AheevaCCS Manager.
   
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_24.png)

   5. Copy and paste the values for "Consumer Key", "Consumer Secret" and the "Callback/Redirect URL" from above and click on the "Connect" button.
   
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_25.png)

   6. A login box might appear. Enter your salesforce login credentials and approve the authentication request.
   
   7. Once authenticated, a list of agents are shown on the screen. You can map the agents inside AheevaCCS with salesforce users and click "Save". Do this for the all the agents that use salesforce.
   
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_26.png)

   8. Once mapped, the agents can login to their Salesforce console and the AheevaCCS Phone will automatically login the corresponding agent in the Agent Application and confirm their default extension.
   
## Creating a Softphone Layout

First time users might need to create a Softphone Layout and assign it to the users in case your Salesforce Environment doesn't come with one already. You can add a Softphone Layout by typing "Softphone Layout" in the "Quick Find" box.

## Samples

### AheevaCCS Phone after completing the setup
   ![](https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/screenshot_27.png)

### Manual Dial using Click to Call
   <https://bitbucket.org/aheeva/salesforce-integration/raw/master/assets/images/manual_dial.mp4>

## Support

For any queries related to the setup/installation, please contact our supoprt team.
